from conan import ConanFile

class MechanicalGenericBenderRecipe(ConanFile):
    name = "mechanicalgenericbender"
    executable = "ds_MechanicalGenericBender"
    version = "1.0.17"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Florent Langlois"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/beamlinecomponents/mirrors/mechanicalgenericbender.git"
    description = "MechanicalGenericBender device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("cpptango/9.2.5@soleil/stable")
        self.requires("yat/[>=1.0]@soleil/stable")
        self.requires("genericbender/[>=1.0]@soleil/stable")
        self.requires("monochromator/[>=1.0]@soleil/stable")
        self.requires("interpolator/[>=1.0]@soleil/stable")
        self.requires("utils/[>=1.0]@soleil/stable")
        self.requires("gsl/1.11@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
