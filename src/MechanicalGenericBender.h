//=============================================================================
//
// file :        MechanicalGenericBender.h
//
// description : Include for the MechanicalGenericBender class.
//
// project :	Mechanical 1,2 or 4 motors generic bender.
//
// $Author: vince_soleil $
//
// $Revision: 1.15 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _MECHANICALGENERICBENDER_H
#define _MECHANICALGENERICBENDER_H


#pragma warning( push )
#pragma warning( disable : 4312 )
#pragma warning( disable : 4311 )
#pragma warning( disable : 4267 )
#pragma warning( disable : 4290 )
#include <tango.h>
#include <TangoExceptionsHelper.h>
#include "DeviceProxyHelper.h"
#pragma warning( pop )

#include "MechanicalBender.h"

#include "OneMotorBender.h"
#include "TwoMotorsBender.h"
#include "FourMotorsBender.h"

/**
 * @author	$Author: vince_soleil $
 * @version	$Revision: 1.15 $
 */

 //	Add your own constants definitions here.
 //-----------------------------------------------


namespace MechanicalGenericBender_ns
{

/**
 * Class Description:
 * This device allows to pilot a mechanical bender with one, two or four motors.
 */

/*
 *	Device States Description:
*  Tango::MOVING :   Bender is MOVING (at least one motor is Moving)
*  Tango::STANDBY :  Bender is STANDBY (all motors are Standby)
*  Tango::ALARM :    Bender is ALARM (at least one motor is Alarm)
*  Tango::FAULT :    Bender is FAULT not possible to readch a device, getting a value or a state.
 *                    (In this case it is only allowed to redone a init after correcting the problem
 *                    with the other devices)
*  Tango::INIT :     The device is in this state after the Init command succeeded.
 *                    After it needs a InitializeBender call to be fully use.
 */


class MechanicalGenericBender: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attributs member data.
 */
//@{
		Tango::DevDouble	*attr_bender_read;
		Tango::DevDouble	attr_bender_write;
		Tango::DevDouble	*attr_bender1_read;
		Tango::DevDouble	attr_bender1_write;
		Tango::DevDouble	*attr_bender2_read;
		Tango::DevDouble	attr_bender2_write;
		Tango::DevDouble	*attr_bender3_read;
		Tango::DevDouble	attr_bender3_write;
		Tango::DevDouble	*attr_bender4_read;
		Tango::DevDouble	attr_bender4_write;
		Tango::DevDouble	*attr_asymmetry_read;
		Tango::DevDouble	*attr_curvature_read;
		Tango::DevDouble	attr_curvature_write;
		Tango::DevDouble	*attr_curvatureRadius_read;
		Tango::DevDouble	attr_curvatureRadius_write;
		Tango::DevDouble	*attr_meanCurvature_read;
		Tango::DevDouble	*attr_meanCurvatureRadius_read;
		Tango::DevShort	*attr_numberOfMotors_read;
		Tango::DevBoolean	*attr_autoSendValues_read;
		Tango::DevBoolean	attr_autoSendValues_write;
//@}

/**
 *	@name Device properties
 *	Device properties member data.
 */
//@{
/**
 *	Give the name of the attribute to move the motor according
 *	the motors device used.
 */
	string	attributePositionName;
/**
 *	Name of the first motor bender to create the proxy to this motor.
 */
	string	bender1MotorName;
/**
 *	Name of the second motor bender to create the proxy to this motor.
 */
	string	bender2MotorName;
/**
 *	Name of the third motor bender to create the proxy to this motor.
 */
	string	bender3MotorName;
/**
 *	Name of the fourth motor bender to create the proxy to this motor.
 */
	string	bender4MotorName;
/**
 *	The bender curvature constant A value used to compute the pseudo bender according
 *	the Rbender value. C=A/R+B
 */
	Tango::DevDouble	benderCurvatureConstantA1;
/**
 *	The bender curvature constant A value used to compute the pseudo bender according
 *	the Rbender value. C=A/R+B
 */
	Tango::DevDouble	benderCurvatureConstantA2;
/**
 *	The bender curvature constant A value used to compute the pseudo bender according
 *	the Rbender value. C=A/R+B
 */
	Tango::DevDouble	benderCurvatureConstantA3;
/**
 *	The bender curvature constant A value used to compute the pseudo bender according
 *	the Rbender value. C=A/R+B
 */
	Tango::DevDouble	benderCurvatureConstantA4;
/**
 *	The bender curvature constant B value used to compute the pseudo bender according
 *	the Rbender value. C=A/R+B
 */
	Tango::DevDouble	benderCurvatureConstantB1;
/**
 *	The bender curvature constant B value used to compute the pseudo bender according
 *	the Rbender value. C=A/R+B
 */
	Tango::DevDouble	benderCurvatureConstantB2;
/**
 *	The bender curvature constant B value used to compute the pseudo bender according
 *	the Rbender value. C=A/R+B
 */
	Tango::DevDouble	benderCurvatureConstantB3;
/**
 *	The bender curvature constant B value used to compute the pseudo bender according
 *	the Rbender value. C=A/R+B
 */
	Tango::DevDouble	benderCurvatureConstantB4;
/**
 *	The index of the first column of the C1 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	c1TableFirstIndex;
/**
 *	The path for the table to compute the C1 value according the Rbender value
 */
	string	c1TablePath;
/**
 *	The index of the second column of the C1 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	c1TableSecondIndex;
/**
 *	The index of the first column of the C2 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	c2TableFirstIndex;
/**
 *	The path for the table to compute the C2 value according the Rbender value
 */
	string	c2TablePath;
/**
 *	The index of the second column of the C2 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	c2TableSecondIndex;
/**
 *	The index of the first column of the C3 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	c3TableFirstIndex;
/**
 *	The path for the table to compute the C3 value according the Rbender value
 */
	string	c3TablePath;
/**
 *	The index of the second column of the C4 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	c3TableSecondIndex;
/**
 *	The index of the first column of the C4 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	c4TableFirstIndex;
/**
 *	The path for the table to compute the C4 value according the Rbender value
 */
	string	c4TablePath;
/**
 *	The index of the second column of the C4 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	c4TableSecondIndex;
/**
 *	Gives the name of the STATE command according the target device.
 *	For SimulatedMotor it is State.
 */
	string	commandStateName;
/**
 *	Name of the command stop for the reached device.
 */
	string	commandStopName;
/**
 *	The maximal value for asymmetry value.
 */
	Tango::DevDouble	maximalAsymmetry;
/**
 *	The maximal bender 1 value.
 */
	Tango::DevDouble	maximalBender1;
/**
 *	The maximal bender 2 value.
 */
	Tango::DevDouble	maximalBender2;
/**
 *	The maximal bender 3 value.
 */
	Tango::DevDouble	maximalBender3;
/**
 *	The maximal bender 4 value.
 */
	Tango::DevDouble	maximalBender4;
/**
 *	The maximal curvature value.
 */
	Tango::DevDouble	maximalCurvature;
/**
 *	The maximal value for the Pseudo Bender value (C value).
 */
	Tango::DevDouble	maximalPseudoBender;
/**
 *	The minimal value for asymmetry value.
 */
	Tango::DevDouble	minimalAsymmetry;
/**
 *	The minimal bender 1 value.
 */
	Tango::DevDouble	minimalBender1;
/**
 *	The minimal bender 2 value.
 */
	Tango::DevDouble	minimalBender2;
/**
 *	The minimal bender 3 value.
 */
	Tango::DevDouble	minimalBender3;
/**
 *	The minimal bender 4 value.
 */
	Tango::DevDouble	minimalBender4;
/**
 *	The minimal curvature value.
 */
	Tango::DevDouble	minimalCurvature;
/**
 *	The minimal value for the Pseudo Bender value (C value).
 */
	Tango::DevDouble	minimalPseudoBender;
/**
 *	The index of the first column of the Rbender1 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbender1TableFirstIndex;
/**
 *	The path for the table to compute the R1 value according the C1 value
 */
	string	rbender1TablePath;
/**
 *	The index of the second column of the Rbender1 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbender1TableSecondIndex;
/**
 *	The index of the first column of the Rbender2 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbender2TableFirstIndex;
/**
 *	The path for the table to compute the R2 value according the C2 value
 */
	string	rbender2TablePath;
/**
 *	The index of the second column of the Rbender2 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbender2TableSecondIndex;
/**
 *	The index of the first column of the Rbender3 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbender3TableFirstIndex;
/**
 *	The path for the table to compute the R3 value according the C3 value
 */
	string	rbender3TablePath;
/**
 *	The index of the second column of the Rbender3 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbender3TableSecondIndex;
/**
 *	The index of the first column of the Rbender4 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbender4TableFirstIndex;
/**
 *	The path for the table to compute the R4 value according the C4 value
 */
	string	rbender4TablePath;
/**
 *	The index of the second column of the Rbender4 table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbender4TableSecondIndex;
/**
 *	The index of the first column of the Rbender table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbenderTableFirstIndex;
/**
 *	The path for the table to compute the R value according the C value
 */
	string	rbenderTablePath;
/**
 *	The index of the second column of the Rbender table.
 *	It is used when the table file contains more than 2 columns
 */
	Tango::DevULong	rbenderTableSecondIndex;
/**
 *	if true, equation are used if false table must be given
 */
	Tango::DevBoolean	useEquation;
/**
 *	The number of motors of the bender.
 */
	Tango::DevShort	numberOfMotors;
/**
 *	The path to the tables
 */
	string	tablesPath;
/**
 *	Set the AutoSend flag to false after each SendValue execution
 */
	Tango::DevBoolean	autoSendAfterWrite;
/**
 *	Value of the AutoSend Flag at Init. (default: false)
 */
	Tango::DevBoolean	autoSendAtInit;
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	MechanicalGenericBender(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	MechanicalGenericBender(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	MechanicalGenericBender(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one desctructor is defined for this class */
//@{
/**
 * The object desctructor.
 */	
	~MechanicalGenericBender() {delete_device();};
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method befor execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name MechanicalGenericBender methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for bender acquisition result.
 */
	virtual void read_bender(Tango::Attribute &attr);
/**
 *	Write bender attribute values to hardware.
 */
	virtual void write_bender(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for bender1 acquisition result.
 */
	virtual void read_bender1(Tango::Attribute &attr);
/**
 *	Write bender1 attribute values to hardware.
 */
	virtual void write_bender1(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for bender2 acquisition result.
 */
	virtual void read_bender2(Tango::Attribute &attr);
/**
 *	Write bender2 attribute values to hardware.
 */
	virtual void write_bender2(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for bender3 acquisition result.
 */
	virtual void read_bender3(Tango::Attribute &attr);
/**
 *	Write bender3 attribute values to hardware.
 */
	virtual void write_bender3(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for bender4 acquisition result.
 */
	virtual void read_bender4(Tango::Attribute &attr);
/**
 *	Write bender4 attribute values to hardware.
 */
	virtual void write_bender4(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for asymmetry acquisition result.
 */
	virtual void read_asymmetry(Tango::Attribute &attr);
/**
 *	Extract real attribute values for curvature acquisition result.
 */
	virtual void read_curvature(Tango::Attribute &attr);
/**
 *	Write curvature attribute values to hardware.
 */
	virtual void write_curvature(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for curvatureRadius acquisition result.
 */
	virtual void read_curvatureRadius(Tango::Attribute &attr);
/**
 *	Write curvatureRadius attribute values to hardware.
 */
	virtual void write_curvatureRadius(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for meanCurvature acquisition result.
 */
	virtual void read_meanCurvature(Tango::Attribute &attr);
/**
 *	Extract real attribute values for meanCurvatureRadius acquisition result.
 */
	virtual void read_meanCurvatureRadius(Tango::Attribute &attr);
/**
 *	Extract real attribute values for numberOfMotors acquisition result.
 */
	virtual void read_numberOfMotors(Tango::Attribute &attr);
/**
 *	Extract real attribute values for autoSendValues acquisition result.
 */
	virtual void read_autoSendValues(Tango::Attribute &attr);
/**
 *	Write autoSendValues attribute values to hardware.
 */
	virtual void write_autoSendValues(Tango::WAttribute &attr);
/**
 *	Read/Write allowed for bender attribute.
 */
	virtual bool is_bender_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for bender1 attribute.
 */
	virtual bool is_bender1_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for bender2 attribute.
 */
	virtual bool is_bender2_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for bender3 attribute.
 */
	virtual bool is_bender3_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for bender4 attribute.
 */
	virtual bool is_bender4_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for asymmetry attribute.
 */
	virtual bool is_asymmetry_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for curvature attribute.
 */
	virtual bool is_curvature_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for curvatureRadius attribute.
 */
	virtual bool is_curvatureRadius_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for meanCurvature attribute.
 */
	virtual bool is_meanCurvature_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for meanCurvatureRadius attribute.
 */
	virtual bool is_meanCurvatureRadius_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for numberOfMotors attribute.
 */
	virtual bool is_numberOfMotors_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for autoSendValues attribute.
 */
	virtual bool is_autoSendValues_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for Stop command.
 */
	virtual bool is_Stop_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for InitializeBender command.
 */
	virtual bool is_InitializeBender_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for SendValues command.
 */
	virtual bool is_SendValues_allowed(const CORBA::Any &any);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * Stop all the bender motors.
 *	@exception DevFailed
 */
	void	stop();
/**
 * This method allows to make the initialization of the four motor bender.
 *	@exception DevFailed
 */
	void	initialize_bender();
/**
 * When "Auto Send Value" attribute is set to false, this command is necessary to send the computed value.
 *	@exception DevFailed
 */
	void	send_values();

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	

	void			updateAllValues(bool bFromWriteCommand);
	void			updateModifiedMotors();

	void			initOneMotorBender();
	void			initTwoMotorsBender();
	void			initFourMotorsBender();
	
	void			initSixteenPiezoBender();

	void			initBender();
	void			deleteBender();

	void			WriteBender1(double dBender1);
	double			ReadBender1();
	Tango::DevState	ReadBender1State();
	
	void			WriteBender2(double dBender2);
	double			ReadBender2();
	Tango::DevState	ReadBender2State();
		
	void			WriteBender3(double dBender3);
	double			ReadBender3();
	Tango::DevState	ReadBender3State();
	
	void			WriteBender4(double dBender4);
	double			ReadBender4();
	Tango::DevState	ReadBender4State();

	bool			ReadAllProxiesStates();
	void			PutAllPointersToZero();
	void			deleteAllObjects();

	Tango::DeviceProxyHelper*			CreateProxy(	std::string sPropertieMotorName,std::string sMotorDescription);
	void			WriteMotor(		Tango::DeviceProxyHelper* mProxyToMotor,std::string sAttributeMotorName,std::string sExceptionDescription,double dMotorValue);
	double			ReadMotor(		Tango::DeviceProxyHelper* mProxyToMotor,std::string sAttributeMotorName,std::string sExceptionDescription);
	double			ReadWriteMotor(	Tango::DeviceProxyHelper* mProxyToMotor,std::string sAttributeMotorName,std::string sExceptionDescription);
	Tango::DevState ReadMotorState(	Tango::DeviceProxyHelper* mProxyToMotor,std::string sAttributeMotorName,std::string sExceptionDescription);

	void			UpdateAllDeviceState();

	void			set_write_value(Tango::DevDouble value, string attribute_name);
	void			CreateAllProperties();

	void			CreateAllProperties(Tango::DbData& dev_prop);
	int				FindIndexFromPropertyName(Tango::DbData& dev_prop, string property_name);
	void			store_value_as_property(Tango::DbData& dev_prop,Tango::DevString value,string property_name);
	void			store_value_as_property(Tango::DevString value, string property_name);

protected :	
	//	Add your own data members here
	//-----------------------------------------
	Tango::DeviceProxyHelper*	_mProxyToBender1Motor;
	Tango::DeviceProxyHelper*	_mProxyToBender2Motor;
	Tango::DeviceProxyHelper*	_mProxyToBender3Motor;
	Tango::DeviceProxyHelper*	_mProxyToBender4Motor;

	Tango::DevState				_mBender1State;
	Tango::DevState				_mBender2State;
	Tango::DevState				_mBender3State;
	Tango::DevState				_mBender4State;

	bool						_bOneMotorBender;
	bool						_bTwoMotorsBender;
	bool						_bFourMotorsBender;

	bool						_bDivideByZeroException;
	bool						_bAllProxiesInitialized;
	bool						_bAllAttributesRead;
	bool						_bBenderInitialized;
	bool						_bAllAttributesCreated;

	stringstream				_sStatusMessage;

	Bender*						_mBender;
	OneMotorBender*				_mOneMotorBender;
	TwoMotorsBender*			_mTwoMotorsBender;
	FourMotorsBender*			_mFourMotorsBender;

	double						_dOldBender1;
	double						_dOldBender2;
	double						_dOldBender3;
	double						_dOldBender4;

	bool						_bRbenderIsOk;
	bool						_bRbenderInvIsOk; 
};

}	// namespace_ns

#endif	// _MECHANICALGENERICBENDER_H
